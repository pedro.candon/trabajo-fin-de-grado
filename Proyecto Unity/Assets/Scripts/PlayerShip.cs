﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerShip : MonoBehaviour {

    public SimpleHealthBar healthBar;
    public SimpleHealthBar shieldBar;
    private int maxHealth = 3000;
    private int daño = 100;
    private int maxShield;
    public static int currentHealth;
    private int currentShield;

    public GameObject deathMenu;

    public GameObject shot;

    private AudioSource audio;

    public AudioClip shotAudio, flyAudio;

    bool isDead;

    // Use this for initialization
    void Start () {
        SumScore.Reset();
        SumScore.HighScore = (int)Usuario.maxScore2;
        
        audio = GetComponent<AudioSource>();
        audio.loop = true;
        /*audio.clip = flyAudio;
        audio.volume = 0.2f;
        audio.Play();*/
        maxShield = maxHealth / 2;
        currentHealth = maxHealth;
        currentShield = maxShield;
        healthBar.UpdateBar(currentHealth, maxHealth);
        healthBar.UpdateBar(currentShield, maxShield);
    }
	
	// Update is called once per frame
	void Update () {
        healthBar.UpdateBar(currentHealth, maxHealth);
        shieldBar.UpdateBar(currentShield, maxShield);

        if(PauseMenu.paused)
            audio.Pause();

        
        if (!isDead && currentHealth <= 0)
        {
            Death();
        }

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.name == "TIEShot(Clone)")
        {
            if (currentShield == 0)
            {
                currentHealth -= daño;
                //Debug.Log("Vida " + currentHealth);
            }
            else
            {
                currentShield -= daño;
                //Debug.Log("Escudo " + currentShield);
            }
        }

    }
    public void shoot()
    {
        //Vector3 pos = new Vector3(transform.position.x, transform.position.y, transform.position.z+0.5f);
        audio.PlayOneShot(shotAudio);
        Instantiate(shot, transform.position, transform.rotation).SetActive(true);
    }


    void Death(){
        isDead = true;
        audio.Stop();
        //SumScore.SaveHighScore();
        //SumScore.Reset();
        if(SumScore.Score>SumScore.HighScore){
            Usuario.maxScore2 = SumScore.Score;
            if(logIn.logged)
                StartCoroutine( Usuario.UpdateScore(2,SumScore.Score) );
        }
        //SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        //Exit();
        deathMenu.SetActive(true);
        Time.timeScale = 0;
    }

    public void Exit()
    {
        SceneManager.LoadScene("menu");
    }

}