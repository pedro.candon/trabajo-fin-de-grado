﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public static class Usuario {

	public const string serverIP="192.168.1.5";

	static string uploadScoreURL="/tfg/insertScore.php";

	public static string id,username="Player"+Random.Range(0,100000), email;

	public static long maxScore1, maxScore2, maxScore3, maxScore4;

	public static long getScore(int gameMode){
		switch(gameMode){
			case 1: return maxScore1;
			case 2: return maxScore2;
			case 3: return maxScore3;
			case 4: return maxScore4;
			default: return 0;
		}
	}

	public static void setScore(int gameMode, long score){
		switch(gameMode){
			case 1: maxScore1=score; break;
			case 2:  maxScore2=score; break;
			case 3:  maxScore3=score; break;
			case 4:  maxScore4=score; break;
			default: return;
		}
	}

	public static IEnumerator UpdateScore(int gameMode, long score){
		WWWForm form=new WWWForm();
		form.AddField("id",Usuario.id);
		form.AddField("game",gameMode);
		form.AddField("score",score.ToString());
		WWW www=new WWW(uploadScoreURL,form);
		yield return www;
		Debug.Log("Upload score: "+www.text);
	}

}
