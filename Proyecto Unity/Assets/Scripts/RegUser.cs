﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RegUser : MonoBehaviour {

	public InputField inputUserName;
	public InputField inputPassword;
	public InputField inputRePassword;
	public InputField inputEmail;

	bool chk=false;
	

	bool registered=false;

	public GameObject errorWindow;

    public Button regButton;

	string createUserURL=Usuario.serverIP+"/tfg/insertuser.php";

	string getUsersEmailURL=Usuario.serverIP+"/tfg/itemsData.php";

	string checkUserURL=Usuario.serverIP+"/tfg/checkUser.php";

	string message;
    private Text errorsText;


	private List<string> emails;

	private List<string> users;

	private string datos;

	bool userCheked, passwordChecked;

	public GameObject loginCanvas, regCanvas;

    IEnumerator Start(){
		errorsText=errorWindow.GetComponentInChildren<Text>();
        errorWindow.SetActive(false);
		emails=new List<string>();
		users=new List<string>();

		WWW itemsData=new WWW(getUsersEmailURL);
		yield return itemsData;
		string itemsDataString=itemsData.text;
		datos=itemsDataString;
		//Debug.Log(datos);
    }

	void validate(){
		message="";

		validateUser();
		validateEmail();

		passwordChecked = validatePassword();

		
	}

	void validateUser(){
		if(inputUserName.text.Equals("")){
			message+="-Nombre de usuario obligatorio\n";
			userCheked = true;
		}else if(!inputEmail.text.Equals("")){
			StartCoroutine(checkUser());
		}
	}

	IEnumerator checkUser(){
		WWWForm form=new WWWForm();
		form.AddField("usernamePost",inputUserName.text.ToLower());
		form.AddField("emailPost",inputEmail.text.ToLower());
		WWW www=new WWW(checkUserURL,form);
		yield return www;
		Debug.Log("Mensaje check user: "+www.text);
		Debug.Log("Conexion errors: "+www.error);

		if( www.text.Contains("error3") || www.error!=null || www.text.Contains("Warning")  ){
			message = "-Error de conexión";

		}else{
			if(www.text.Contains("error2")){
				message+="-Email ya usado\n\n";
			}

			if(www.text.Contains("error1")){
				message+="-Nombre de usuario no disponible\n";
			}
		}

		userCheked=true;
    }

	bool validatePassword(){
		bool ok=true;

		if(inputPassword.text.Equals("")){
			message+="-Contraseña obligatoria\n";
			ok=false;
		}else{
		
			if(inputPassword.text.Length<8){
				message+="-La contraseña debe tener 8 caracteres\n";
				ok=false;
			}
			if(!formatPassword()){
				message+="-La contraseña debe contener números\n";
				ok=false;
			}
			if(!inputPassword.text.Equals(inputRePassword.text)){
				message+="-Las contraseñas no coinciden\n";
				ok=false;
			}
		}


		return ok;
	}

	bool formatPassword(){

		string pattern="[0-9]";

		Match m = Regex.Match(inputPassword.text, pattern);

		if(m.Success){
			return true;
		}else return false;
	}

	// Use this for initialization
	public void RegisterUser () {
		validate();
		if(!message.Equals(""))
			showErrorPanel(true);
		else
			StartCoroutine(RegUserWait());
	}

	IEnumerator RegUserWait(){
		yield return new WaitUntil(()=> userCheked && passwordChecked);
		if(message.Equals("")){
			Debug.Log("Entro");
			StartCoroutine( CreateUser(inputUserName.text,inputPassword.text,inputEmail.text));  
		}
		else{
			Text errorsText=errorWindow.GetComponentInChildren<Text>();
			errorsText.text=message;
            showErrorPanel(true);
        }
	}

    public void showErrorPanel(bool b){

		Debug.Log("M: "+message);
        
        errorWindow.SetActive(b);
		errorsText.text=message.Equals("")?"Usuario registrado":message;
		if(message.Equals("")){
			registered=true;
			//logIn.logged=true;
			regButton.gameObject.SetActive(false);
			logIn.user=inputUserName.text;
		}else{
			userCheked = false;
			passwordChecked = false;
		}
    }

	IEnumerator CreateUser(string username, string password,string email){
		WWWForm form=new WWWForm();
		form.AddField("usernamePost",username);
		form.AddField("passwordPost",password);
		form.AddField("emailPost",email);
		WWW www=new WWW(createUserURL,form);
		yield return www;
		showErrorPanel(true);
	}

	public void Exit(){
        SceneManager.LoadScene("sign-in");
    }

	public void Login()
    {
		if(registered){

			loginCanvas.SetActive(true);
			regCanvas.SetActive(false);
        	//SceneManager.LoadScene("login");
		}else
			errorWindow.SetActive(false);
    }

	void validateEmail()
	{
		
		if(inputEmail.text.Equals("")){
			message+="-Email obligatorio\n";
		}else{
			
			string email=inputEmail.text;
			try {
				Regex rx = new Regex(
			@"^[-!#$%&'*+/0-9=?A-Z^_a-z{|}~](\.?[-!#$%&'*+/0-9=?A-Z^_a-z{|}~])*@[a-zA-Z](-?[a-zA-Z0-9])*(\.[a-zA-Z](-?[a-zA-Z0-9])*)+$");
				bool v=rx.IsMatch(email);
				if(!v)
					message+="-Email no válido\n";
			}
			catch {
				message+="-Email no válido\n";
			}
		}
	}

	public void ResetFields(){
		inputUserName.text="";
		inputEmail.text="";
		inputPassword.text="";
		inputRePassword.text="";
	}
}
