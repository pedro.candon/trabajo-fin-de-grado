﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Vuforia;

public class ShipSelection : MonoBehaviour {

    public GameObject playButton;

    bool selected;

    public Canvas selectionCanvas, menuCannvas, singinCanvas;

    public static AsyncOperation loadScene;

    IEnumerator Start(){
        playButton.SetActive(false);
        selected = false;
        yield return null;
        loadScene = SceneManager.LoadSceneAsync("minirace", LoadSceneMode.Additive);
        loadScene.allowSceneActivation = false;
    }

    

    private void Update() {
        //VideoBackgroundBehaviour bgPlane = Camera.main.GetComponent<VideoBackgroundBehaviour> ();
        //VuforiaBehaviour vufBehav = Camera.main.GetComponent<VuforiaBehaviour> ();
        //VideoBackgroundBehaviour bgPlane = Camera.main.GetComponent<VideoBackgroundBehaviour> ();
        /*if (bgPlane.enabled) {
            // switch it off
            bgPlane.enabled = false;
            vufBehav.enabled = false;
        }*/
        
    }

    public void selectShip(int id){
        PodRacer.shipID = id;
        playButton.SetActive(true);
    }

    public void loadMinirace(){
        selectionCanvas.gameObject.SetActive(false);
        
        menuCannvas.enabled = false;
        singinCanvas.enabled = false;
        //SceneManager.LoadSceneAsync("minirace");
        loadScene.allowSceneActivation = true;
        GetComponent<AudioSource>().Stop();
    }

    public void backtoMenu(){
        //SceneManager.LoadScene("menu");
        menuCannvas.gameObject.SetActive(true);
        selectionCanvas.gameObject.SetActive(false);
    }

	
}
