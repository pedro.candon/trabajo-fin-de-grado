﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC : MonoBehaviour {

	public GameObject model,modelGhost;

	private AudioSource audioSource;

	public AudioClip correctSound, wrongSound;

	// Use this for initialization
	void Start () {
		model.SetActive(false);
		audioSource = GetComponent<AudioSource>();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	/// <summary>
	/// OnTriggerEnter is called when the Collider other enters the trigger.
	/// </summary>
	/// <param name="other">The other Collider involved in this collision.</param>
	void OnTriggerEnter(Collider other)
	{
		Debug.Log(name+" con "+other.name);
		if(other.name.ToLower().Contains(model.name.ToLower())){
			switch(name){
				case "npc1": DioramaManager.npc1=true; break;
				case "npc2": DioramaManager.npc2=true; break;
				case "npc3": DioramaManager.npc3=true; break;
				case "npc4": DioramaManager.npc4=true; break;
				case "npc5": DioramaManager.npc5=true; break;
				case "npc6": DioramaManager.npc6=true; break;
				case "npc7": DioramaManager.npc7=true; break;
				default:  break;
				
			}
			audioSource.Stop();
			audioSource.PlayOneShot(correctSound, 0.1f);
			
			model.SetActive(true);
			modelGhost.SetActive(false);
			
		}else{
			DioramaManager.intentos++;
			audioSource.Stop();
			audioSource.PlayOneShot(wrongSound, 0.1f);
		}
	}

	private void OnCollisionEnter(Collision other) {
		Debug.Log(other.collider.name);
	}
}
