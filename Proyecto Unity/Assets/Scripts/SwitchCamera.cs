﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Vuforia;

public class SwitchCamera : MonoBehaviour {

    public Camera ARcam;
    public Camera[] npcCam;

    private int selectedCam = 0;

    /// <summary>
    /// Start is called on the frame when a script is enabled just before
    /// any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        foreach(Camera c in npcCam){
            c.enabled = false;
        }
    }

    public void SwitchCam(int i){
        if(i==0){
            DefaultTrackableEventHandler.render = false;
            TrackerManager.Instance.GetTracker<ObjectTracker>().Start();
        }else{
            DefaultTrackableEventHandler.render = true;
            TrackerManager.Instance.GetTracker<ObjectTracker>().Stop();
        }
        selectedCam = i;
        ARcam.enabled = false;

        foreach(Camera c in npcCam){
            c.enabled = false;
        }
        if(i>0)
            npcCam[i-1].enabled = true;
        else
            ARcam.enabled = true;

            Debug.Log("Camara cambiada "+selectedCam);
    }

    private void Update()
    {
        if(selectedCam>0)
            npcCam[selectedCam-1].transform.rotation = ARcam.transform.rotation;
        //Vadercam.transform.rotation = ARcam.transform.rotation;
        //Bencam.transform.rotation = ARcam.transform.rotation;
    }


    public void Exit()
    {
        SceneManager.LoadScene("menu");
    }
}
