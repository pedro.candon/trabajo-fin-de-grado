﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Vuforia;

public class PodRacer : Photon.MonoBehaviour, IPunObservable {

    public static int shipID;
    GameObject pcIndicator;

    private Transform[] startPositions;

    public int startPosition;

    Transform ground;

    public string racerName;

    public Text lapText;

    private Slider sliderLeft;
    private Slider sliderRight;
    private GameObject target;
    private GameObject meta;

    int lap = 0;

    int checkpoint = 0;

    Transform[] checkPoints;
    public Transform[] gravityPoints;
    private Rigidbody body;
    public float hoverForce=2f;
    public float hoverHeight;
    public float speed = 1f;
    private SimpleTouchController leftController;

    private AudioSource audioSource;

    public AudioClip impactSound, lapSound;

    // Use this for initialization
    void Start () {
        audioSource = GetComponent<AudioSource>();
        pcIndicator = GameObject.Find("PCIndicator");
        
        leftController = GameObject.FindObjectOfType<SimpleTouchController>();
        meta = GameObject.Find("A");
        transform.position = meta.transform.position;
        body = GetComponent<Rigidbody>();
        lapText = GameObject.Find("LapNum").GetComponent<Text>();
        racerName = Usuario.username;
        if(photonView.isMine){
            photonView.RPC ("AddRacerRPC", PhotonTargets.All, PhotonNetwork.player.NickName);
        }

        checkPoints = GameObject.Find("Checkpoints").GetComponentsInChildren<Transform>();
        
        

    }

    // Update is called once per frame
    void Update () {
        //Debug.Log(isLocalPlayer);
       /* if (!isLocalPlayer)
            return;*/
        if(PhotonNetworkManager.scanned){
            startPositions = GameObject.Find("StartPosPoints").GetComponentsInChildren<Transform>();
            if(photonView.isMine){

                Vector3 indicatorPos = transform.position;
                indicatorPos.y+=1.5f;
                pcIndicator.transform.position = indicatorPos;

                if(!PhotonNetworkManager.raceStarted){
                    if(PhotonNetwork.player.ID>=startPositions.Length){
                        transform.position = startPositions[ 4].position;
                        transform.rotation = startPositions[ 4].rotation;
                    }else{
                        transform.position = startPositions[ PhotonNetwork.player.ID].position;
                        transform.rotation = startPositions[ PhotonNetwork.player.ID].rotation;
                    }
                    
                    GetComponent<Rigidbody>().useGravity = false;
                }else{
                    GetComponent<Rigidbody>().useGravity = true;

                    if(checkHigh()<=0){
                        Debug.Log("Respawn cp: "+checkpoint);
                        transform.position = checkPoints[checkpoint].position;
                        transform.rotation = checkPoints[checkpoint].rotation;
                        gameObject.GetComponent<Rigidbody>().velocity=Vector3.zero;
                    }
                }

                
                if(checkPoints!=null)
                    photonView.RPC ("SyncPosRPC", PhotonTargets.All, PhotonNetwork.player.NickName);
                if(lap<=PhotonNetworkManager.numLaps)
                    lapText.text = lap+"/"+PhotonNetworkManager.numLaps;
                
                float x = leftController.GetTouchPosition.x;
                float y = leftController.GetTouchPosition.y;
                if(Mathf.Atan2(x, y)!=0)
                    transform.rotation = Quaternion.Euler(0, Mathf.Atan2(x, y) * Mathf.Rad2Deg + 90,0);
            }
        }
    }

    [PunRPC]
	void SyncPosRPC (string name) {
        int cp = checkpoint;
        cp++;
        if(checkPoints!=null){
            if(cp>=checkPoints.Length)
                cp=1;
            PhotonNetworkManager.SyncLap(name, lap, checkpoint, Vector3.Distance(transform.position, checkPoints[cp].position) );
            
        }
		
	}

    [PunRPC]
	void AddRacerRPC (string name) {
		PhotonNetworkManager.AddRacer(name);
	}

    private void FixedUpdate()
    {
        if(photonView.isMine){
        /* if (!isLocalPlayer)
                return;*/
            float x = leftController.GetTouchPosition.x;
            float y = leftController.GetTouchPosition.y;
            var direction = new Vector3(x, 0, y);
            var rotation = new Vector3(y, 0, -x);
            body.AddForce(transform.forward * direction.magnitude*speed);
            rotation *= Time.deltaTime * speed * (2 * Mathf.PI * transform.localScale.magnitude) * 2;
            // and rotate by the rotation
            //transform.rotation = Quaternion.Euler(270, 180, Mathf.Atan2(x, y) * Mathf.Rad2Deg + 90);
            
                //transform.eulerAngles = new Vector3(0, Mathf.Atan2(x,y) * 180 / Mathf.PI, 0);
            // transform.eulerAngles = new Vector3(transform.eulerAngles.x, Mathf.Atan2(x, y) * Mathf.Rad2Deg, transform.eulerAngles.z);


            hover3();
        }

        
    }

    void Hover1()
    {
        Transform hoverPoint;
        // body.AddForceAtPosition(hoverPoint.transform.up * (hoverForce-50), hoverPoint.transform.position);
        RaycastHit hit;
        for (int i = 0; i < gravityPoints.Length; i++)
        {
            hoverPoint = gravityPoints[i];

            if (Physics.Raycast(hoverPoint.transform.position, -Vector3.up, out hit, hoverHeight))
            {
                float proportionalHeight = (hoverHeight - hit.distance) / hoverHeight; ;// 1f -(hit.distance) / hoverHeight;
                Vector3 appliedHoverForce = Vector3.up * proportionalHeight * hoverForce;

                body.AddForce(appliedHoverForce, ForceMode.Acceleration);
                body.AddForceAtPosition(appliedHoverForce, hoverPoint.transform.position, ForceMode.Acceleration);
                

            }

            
        


        }

        Vector3 impulseForce= body.transform.forward * hoverForce * speed * sliderLeft.value;
        body.AddForceAtPosition(impulseForce, gravityPoints[0].transform.position);

        impulseForce = body.transform.forward * hoverForce * speed * sliderRight.value;
        body.AddForceAtPosition(impulseForce, gravityPoints[1].transform.position);
    }

    void Hover2()
    {
        Ray ray = new Ray(transform.position, -transform.up);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, hoverHeight))
        {
            float proportionalHeight = (hoverHeight - hit.distance) / hoverHeight;
            Vector3 forceDown = Vector3.down* proportionalHeight * hoverForce;
            Vector3 forceUp = Vector3.up * proportionalHeight * hoverForce;
            //body.AddForce(forceDown, ForceMode.Acceleration);
            body.AddForce(forceUp, ForceMode.Acceleration);
        }
        for (int i = 0; i < gravityPoints.Length; i++) { 
            Vector3 impulseForce = gravityPoints[i].transform.forward * hoverForce * speed;
        }
        //body.AddRelativeForce(0f, 0f, powerInput * speed);
        //body.AddRelativeTorque(0f, turnInput * turnSpeed, 0f);

    }

    void hover3()
    {
        Ray ray = new Ray(transform.position, -transform.up);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, hoverHeight))
        {
            float proportionalHeight = (hoverHeight - hit.distance) / hoverHeight;
            Vector3 forceDown = Vector3.down * proportionalHeight * hoverForce;
            Vector3 forceUp = Vector3.up * proportionalHeight * hoverForce;
            //body.AddForce(forceDown, ForceMode.Acceleration);
            body.AddForce(forceUp, ForceMode.Acceleration);
        }

    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.name.Equals("A"))
        {
            
            if (lap == 0 || checkpoint==4)
            {
                lap++;
                checkpoint=1;
                if(photonView.isMine){
                    audioSource.volume = 1;
                    audioSource.PlayOneShot(lapSound,0.2f);
                }
                
            }
            
        }
        else if (other.name.Equals("B"))
        {
            
            if (checkpoint==1)
            {
                checkpoint=2;
            }
        }
        else if (other.name.Equals("C"))
        {
            
            if (checkpoint==2)
            {
                checkpoint=3;
            }
        }
        else if (other.name.Equals("D"))
        {
            
            if (checkpoint==3)
            {
                checkpoint=4;
            }
            
        }
    }

    float checkHigh(){

        return transform.position.y;
    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        //throw new System.NotImplementedException();
    }

    /// <summary>
    /// OnCollisionEnter is called when this collider/rigidbody has begun
    /// touching another rigidbody/collider.
    /// </summary>
    /// <param name="other">The Collision data associated with this collision.</param>
    void OnCollisionEnter(Collision other)
    {
        audioSource.volume = 0.2f;
        audioSource.PlayOneShot(impactSound,0.1f);
    }
}
