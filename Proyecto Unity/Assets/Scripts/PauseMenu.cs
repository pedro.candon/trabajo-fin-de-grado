﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour {


    public static bool paused = false;
    public GameObject UI;

    public GameObject pauseMenu, tutorial;

    private bool tutorialPassed;


    // Use this for initialization
    void Start () {
        pauseMenu.SetActive(false);
        if(tutorial!=null){
            paused = true;
            Time.timeScale=0;
        }
        //UI= GameObject.Find("UI");
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void PassTutorial(){
        tutorialPassed = true;
        Time.timeScale = 1;
        paused = false;
    }

    public void pause()
    {
        if (!paused)
        {
            Time.timeScale = 0;
            paused = true;
            if(tutorial!=null && !tutorialPassed){
                tutorial.SetActive(false);
            }
        }
        else
        {
            Time.timeScale = 1;
            paused = false;
            if(tutorial!=null && !tutorialPassed){
                tutorial.SetActive(true);
            }

        }
        UI.SetActive(!paused);
        pauseMenu.SetActive(paused);
        Debug.Log("Paused: "+paused);
        
    }
    public void pause(bool p){
        if (p)
        {
            Time.timeScale = 0;
        }
        else
        {
            Time.timeScale = 1;

        }
    }

    public void Exit()
    {
        SceneManager.LoadScene("menu");
        paused = false;
    }

    public void DestroyMiniRaceScene(){
        StartCoroutine(DestroyMiniRaceSceneCoroutine());
    }

    IEnumerator DestroyMiniRaceSceneCoroutine(){
        AsyncOperation unloadOP=SceneManager.UnloadSceneAsync("miniRace");
        yield return new WaitUntil(() => unloadOP.isDone);
        //ShipSelection.loadScene=SceneManager.LoadSceneAsync("minirace", LoadSceneMode.Additive);
        //ShipSelection.loadScene.allowSceneActivation = false;
    }
}
