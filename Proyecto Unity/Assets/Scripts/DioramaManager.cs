﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class DioramaManager : MonoBehaviour {

	public DefaultTrackableEventHandler tracker;

	public Canvas markerScanCanvas;

	int puntos=500;

	public static int intentos;

	public static bool npc1,npc2,npc3,npc4,npc5,npc6, npc7;

	public GameObject stage1, stage2, stage3, stage4, stage5, endGame;

	private bool nextStage;

	public GameObject nextStageBut, endGameUI, UI;

	private AudioSource audioSource;

	public Canvas stage1Canvas, stage2Canvas, stage3Canvas, stage4Canvas, stage5Canvas;

	public SwitchCamera switchCam;

	public Text puntuacion, hScore;

	public Material mat1, mat2,mat3,mat4,mat5;

	public AudioClip r2d2;

	// Use this for initialization
	void Start () {
		SumScore.Reset();
		audioSource = GetComponent<AudioSource>();
		SumScore.HighScore = (int)Usuario.maxScore4;
		hScore.text  = Usuario.maxScore4.ToString();
		intentos=1;
		nextStage = false;
		npc1 = false;
		npc2 = false;
		npc3 = false;
		npc4 = false;
		npc6 = false;
		npc5 = false;
		npc7 = false;
		nextStageBut.SetActive(false);
		stage1.SetActive(false);
		stage2.SetActive(false);
		stage3.SetActive(false);
		stage4.SetActive(false);
		stage5.SetActive(false);
		stage1Canvas.gameObject.SetActive(false);
		stage2Canvas.gameObject.SetActive(false);
		stage3Canvas.gameObject.SetActive(false);
		stage4Canvas.gameObject.SetActive(false);
		stage5Canvas.gameObject.SetActive(false);
		endGame.SetActive(false);
		endGameUI.SetActive(false);

		stage1.transform.Find("SandBox").gameObject.SetActive(false);
		stage2.transform.Find("SandBox").gameObject.SetActive(false);
		stage3.transform.Find("SandBox").gameObject.SetActive(false);
		stage4.transform.Find("SandBox").gameObject.SetActive(false);
		stage5.transform.Find("SandBox").gameObject.SetActive(false);
	}

	public void LoadNextStage(){
		nextStage = true;
	}
	
	// Update is called once per frame
	void Update () {

		if(tracker.scanned){
			markerScanCanvas.enabled=false;
		}

		if(npc1 && npc2 && stage1.activeSelf){
			RenderSettings.skybox = mat1;
			nextStageBut.SetActive(true);
			stage1Canvas.gameObject.SetActive(true);
			stage1.transform.Find("SandBox").gameObject.SetActive(true);
			if(nextStage){
				stage1.transform.Find("SandBox").gameObject.SetActive(false);
				stage1Canvas.gameObject.SetActive(false);
				switchCam.SwitchCam(0);
				SumScore.Add(puntos/intentos);
				stage1.SetActive(false);
				stage2.SetActive(true);
				intentos=1;
				nextStage = false;
				nextStageBut.SetActive(false);
				//carga siguiente escenario
			}
		}

		if(npc3 && stage2.activeSelf){
			RenderSettings.skybox = mat2;
			nextStageBut.SetActive(true);
			stage2Canvas.gameObject.SetActive(true);
			stage2.transform.Find("SandBox").gameObject.SetActive(true);
			if(nextStage){
				stage2.transform.Find("SandBox").gameObject.SetActive(false);
				stage2Canvas.gameObject.SetActive(false);
				switchCam.SwitchCam(0);
				SumScore.Add(puntos/intentos);
				stage2.SetActive(false);
				stage3.SetActive(true);
				//carga siguiente escenario
				intentos=1;
				nextStage = false;
				nextStageBut.SetActive(false);
			}
		}

		if(npc7 && stage3.activeSelf){
			RenderSettings.skybox = mat3;
			nextStageBut.SetActive(true);
			stage3Canvas.gameObject.SetActive(true);
			stage3.transform.Find("SandBox").gameObject.SetActive(true);
			if(nextStage){
				stage3.transform.Find("SandBox").gameObject.SetActive(false);
				stage3Canvas.gameObject.SetActive(false);
				switchCam.SwitchCam(0);
				SumScore.Add(puntos/intentos);
				stage3.SetActive(false);
				stage4.SetActive(true);
				//carga siguiente escenario
				intentos=1;
				nextStage = false;
				nextStageBut.SetActive(false);
			}
		}

		if(npc5 && npc6 && stage4.activeSelf){
			RenderSettings.skybox = mat4;
			stage4Canvas.gameObject.SetActive(true);
			nextStageBut.SetActive(true);
			stage4.transform.Find("SandBox").gameObject.SetActive(true);
			if(nextStage){
				stage4.transform.Find("SandBox").gameObject.SetActive(false);
				stage4Canvas.gameObject.SetActive(false);
				switchCam.SwitchCam(0);
				SumScore.Add(puntos/intentos);
				stage4.SetActive(false);
				stage5.SetActive(true);
				//carga siguiente escenario
				intentos=1;
				nextStage = false;
				nextStageBut.SetActive(false);
			}
		}
		//Escena final---------------------------
		if(npc4 && stage5.activeSelf){
			RenderSettings.skybox = mat5;
			nextStageBut.SetActive(true);
			stage5Canvas.gameObject.SetActive(true);
			stage5.transform.Find("SandBox").gameObject.SetActive(true);
			if(nextStage){
				stage5.transform.Find("SandBox").gameObject.SetActive(false);
				stage5Canvas.gameObject.SetActive(false);
				switchCam.SwitchCam(0);
				SumScore.Add(puntos/intentos);
				stage5.SetActive(false);
				endGame.SetActive(true);
				puntuacion.text = "Puntos: "+SumScore.Score.ToString();
				endGameUI.SetActive(true);
				UI.SetActive(false);
				//carga siguiente escenario
				intentos=1;
				nextStage = false;
				nextStageBut.SetActive(false);
				audioSource.Stop();
				audioSource.PlayOneShot(r2d2,0.5f);
				
			}
			
			
		}
		
	}

	public void ResetDiorama(){
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

	public void Exit(){
		Debug.Log("Score: "+SumScore.Score);
		Debug.Log("HighScore: "+SumScore.HighScore);
		if(SumScore.Score>Usuario.maxScore4){
			Usuario.maxScore4 = SumScore.Score;
			if(logIn.logged)
				StartCoroutine( Usuario.UpdateScore(4,SumScore.Score) );
		}
		SceneManager.LoadScene("menu");
	}
}
