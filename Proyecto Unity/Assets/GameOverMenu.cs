﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverMenu : MonoBehaviour {

	string uploadScoreURL="http://localhost/tfg/insertScore.php";

	public Text score;

	public Text achievedScore;

	public int gameMode;

	private void Update()
	{
		achievedScore.text = "Puntuación: "+SumScore.Score.ToString();
	}


	public void reset()
	{
		SceneManager.LoadScene( SceneManager.GetActiveScene().buildIndex ) ;
	}

	public void exit(){
		SceneManager.LoadScene("menu");
	}
}
