﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Vuforia;
using System.Linq;


public class PhotonNetworkManager : Photon.MonoBehaviour {
	
	float countDown=4;

	bool instantiate;

	public static bool scanned;

	bool startCountdown = false;

	public GameObject imageTarget;

	static List<String> positions;

	public DefaultTrackableEventHandler tracker;

	public Canvas markerScanCanvas;

	public static bool gameOver;

	static Dictionary<String, double> racers;

	GameObject clone;

	public Text positionsT;

	public Text gameOverT;

	public GameObject gameoverPanel;

	static int cloneNumber=1;

	public int shipNum;

	GameObject player;

	public int numPlayers;

	public static int numLaps = 3;

	[SerializeField] private Text connectedText;

	static string winner;

	static public Text gameOverMsg;

	[SerializeField] private Text posText;
	[SerializeField] private Text nickname;
	[SerializeField] public GameObject vader;
	[SerializeField] public GameObject falcon;

	[SerializeField] public GameObject tie;

	[SerializeField] public GameObject xwing;

	[SerializeField] public GameObject pod;

	[SerializeField] public GameObject arc;

	[SerializeField] public Transform spawnPoint;

	public static bool allReady = false;

	public static bool raceStarted = false;

	private Text startMsg;

	private bool scoreUpdated;

	PodRacer playerPod;

	private AudioSource audioSource;

	public AudioClip countDownSound, startRaceSound, ambienceMusic; 

	public ScrollRect Joystick;

	private int previousSec;

	// Use this for initialization
	void OnEnable () {
		gameOver = false;
		allReady = false;
		Joystick.enabled = false;
		
		

		gameoverPanel.SetActive(false);
		Time.timeScale = 1;
		Debug.Log("-----------Escena activa "+SceneManager.GetSceneByName("minirace").name );
		SceneManager.SetActiveScene(SceneManager.GetSceneByName("minirace"));
		previousSec = 4;
		audioSource = GetComponent<AudioSource>();
		//audioSource.PlayOneShot(ambienceMusic);
		connectedText.gameObject.SetActive(true);
		nickname.gameObject.SetActive(true);
		startMsg =GameObject.Find("STARTmsg").GetComponent<Text>();
		startMsg.gameObject.SetActive(true);
		MixedRealityController.Instance.SetMode(MixedRealityController.Mode.HANDHELD_AR);

		racers = new Dictionary<string, double>();
 
		VuforiaARController.Instance.SetWorldCenterMode(VuforiaARController.WorldCenterMode.FIRST_TARGET);
		PhotonNetwork.ConnectUsingSettings("racer");
        System.Random rand =new System.Random();
		
		positions = new List<string>();

		gameoverPanel.SetActive(false);


	}

	void loadShip(){
		switch(shipNum){
			case 1: player=vader; break;
			case 2: player=falcon; break;
			case 3: player=xwing; break;
			case 4: player=tie; break;
			case 5: player=pod; break;
			case 6: player=arc; break;
			default: player=pod; break;
		}
	}

	public static int GetPos(){
		for(int i=0;i<positions.Count;i++){
			if(positions[i].Equals(Usuario.username))
				return i+1;
		}

		return 0;
	}

	public virtual void OnConnectedToMaster(){
		Debug.Log("Connected to master");
		
	}

	[PunRPC]
	void SendMessageRPC (string message) {
		//chatBox.text+="\n"+message;
	}

	

	public virtual void OnJoinedLobby(){
		PhotonNetwork.player.NickName=Usuario.username;
		PhotonNetwork.player.UserId = PodRacer.shipID.ToString();
		//PhotonNetwork.RPC ("SendMessageRPC", PhotonTargets.Others, PhotonNetwork.player.NickName+" joined to lobby!");
		//if(PhotonNetwork.playerList.Length<numPlayers)
			PhotonNetwork.JoinOrCreateRoom("PodRaceRoom",null,null);
		//else
			//PhotonNetwork.JoinOrCreateRoom("PodRaceRoom"+(PhotonNetwork.countOfRooms),null,null);
		
		//PhotonNetwork.player.NickName=SystemInfo.deviceName;
		
		
	}

	public virtual void OnJoinedRoom(){
			Debug.Log("Joined to room: "+PhotonNetwork.room.Name);
			Debug.Log(photonView);
			nickname.text="Nickname: "+PhotonNetwork.player.NickName;
			shipNum = int.Parse(PhotonNetwork.player.UserId);
			loadShip();
			
			
			//racers.Add(PhotonNetwork.player.NickName, clone);
			playerPod = player.GetComponent<PodRacer>();
			
			
			//photonView.RPC ("SendMessageRPC", PhotonTargets.Others, PhotonNetwork.player.NickName+" joined to room!");
		
	}

	void OnLeftRoom()
    {
		PhotonNetwork.LeaveRoom();
        PhotonNetwork.LoadLevel(0);
    }


	

	public static void AddRacer(string name){
		racers.Add(name,0);
	}
	public static void SyncLap(string name, int lap, int checkpoint, double dist){
		if(lap==numLaps+1){
			Time.timeScale=0;
				//Debug.Log(name+" ha ganado!");
				winner = name;
				gameOver = true;
		}else{
			racers[name] = lap*100 + checkpoint*10 - dist;
			Debug.Log(name+" vuelta: "+lap+" cp: "+checkpoint);
			List<string> tempPos=new List<string>();

			foreach(KeyValuePair<string, double> racer in racers.OrderBy(key => key.Value))  
			{  
				tempPos.Add(racer.Key);
			}

			positions = tempPos;

			positions.Reverse();
		}
	}

	void HideStartmsg(){
		startMsg.gameObject.SetActive(false);
	}
	

	// Update is called once per frame
	void Update () {

		if(tracker.scanned){
			scanned = true;
			markerScanCanvas.enabled=false;

			Debug.Log("Already: "+allReady);
			if(allReady){
				if(!instantiate){
					GameObject clone = PhotonNetwork.Instantiate(player.name,spawnPoint.position,Quaternion.identity,0);
					//clone.transform.parent = imageTarget.transform;
					clone.GetComponent<PodRacer>().startPosition = PhotonNetwork.player.ID;
					Debug.Log("PLAYERID: "+PhotonNetwork.player.ID);
					instantiate = true;
				}

				countDown -= Time.deltaTime;
				if(countDown < 1)
				{
					if(!raceStarted)
						audioSource.PlayOneShot(startRaceSound);
					
					startMsg.text = "¡A correr!";
					raceStarted = true;
					Joystick.enabled = true;
					Invoke("HideStartmsg",1);
				}else{
					if(previousSec!=(int)countDown){
						previousSec=(int)countDown;
						audioSource.PlayOneShot(countDownSound);
					}
					startMsg.text = ((int)countDown).ToString();
				}
			}
			
			connectedText.text="Conexión: "+PhotonNetwork.connectionStateDetailed.ToString();

			Debug.Log("gameOver: "+gameOver);

			if(gameOver){
				if(!winner.Equals(Usuario.username))
					gameOverT.text = "¡"+winner+" ha ganado!\nHas quedado "+posText.text;
				else{
					gameOverT.text = "¡Has ganado!";
					if(!scoreUpdated){
						scoreUpdated= true;
						Usuario.maxScore3++;
						if(logIn.logged)
							StartCoroutine( Usuario.UpdateScore(3,Usuario.maxScore3) );
					
					}
					
				}
				gameoverPanel.SetActive(true);
				audioSource.Stop();
			}

			
			positionsT.text="Posiciones:\n";
			for(int i=0;i<positions.Count;i++){
				positionsT.text+=((i+1)+"º-"+positions[i]+"\n");
				if(positions[i].Equals(Usuario.username))
					posText.text=(i+1)+"º";
			}


			if(!allReady){
				if(PhotonNetwork.playerList.Length<numPlayers){
					startMsg.text = "Esperando corredores...";
					Time.timeScale=0;
				}else{
					GameObject[] gobjs = GameObject.FindGameObjectsWithTag("Players");
					foreach(GameObject g in gobjs){
						g.transform.parent = imageTarget.transform;
					}
					Time.timeScale=1;
					startCountdown = true;
					startMsg.text="Preparados";
					Invoke("StartRace",3);
				}
				
			}
		}

		
	}

	void StartRace(){
		allReady=true;
		
	}


	public void exit(){
		
		PhotonNetwork.Disconnect ();
		tracker.scanned = false;
		scanned = false;
		
		StartCoroutine (DoSwitchLevel());
		
	}
	IEnumerator DoSwitchLevel ()
	{
		PhotonNetwork.Disconnect ();
		while (PhotonNetwork.connected)
			yield return null;
		//SceneManager.LoadSceneAsync("miniRace");
		OnLeftRoom();
		
		
	}
}
