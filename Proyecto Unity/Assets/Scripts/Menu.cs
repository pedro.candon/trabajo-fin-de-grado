﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.XR;


public class Menu : MonoBehaviour {

    string sqlServer= "localhost";

    public Text labelLog;

    public GameObject logImg;

    private AudioSource audioSource;

    public AudioClip selectAudio;

    public Canvas canvasShipSelector, canvasStats, canvasSign;


	// Use this for initialization
	void Start () {
        if(canvasShipSelector!=null)
            canvasShipSelector.gameObject.SetActive(false);

        
		//XRSettings.enabled = false;
        
        
        if(SceneManager.GetActiveScene().buildIndex == 0){
            if(logIn.logged){
                logImg.GetComponent<Image>().color = Color.green;
                labelLog.text=Usuario.username ;
            }else{
                labelLog.text="CONECTAR";
                logImg.GetComponent<Image>().color = Color.red;
            }
            audioSource = GetComponent<AudioSource>();
            audioSource.Play();

        }
	}
	
	public void SelectSound(){
        audioSource.PlayOneShot(selectAudio);
    }

    public void CheckStatus(){
        if(logIn.logged){
                logImg.GetComponent<Image>().color = Color.green;
                labelLog.text=Usuario.username ;
        }else{
            labelLog.text="CONECTAR";
            logImg.GetComponent<Image>().color = Color.red;
        }
    }

    public void loadBB8()
    {
        SceneManager.LoadScene("BB8");
    }

    public void loadXwing()
    {
        SceneManager.LoadScene("turret");
    }

    public void loadDiorama()
    {
        SceneManager.LoadScene("diorama");
    }

    public void loadRace()
    {
        canvasShipSelector.gameObject.SetActive(true);
    }

    public void loadSignIn(){
        Debug.Log("loadsignin");
        if(logIn.logged){
            //SceneManager.LoadScene("stats");
            GetComponent<StatsManager>().CheckScores();
            canvasStats.gameObject.SetActive(true);
        }else{
            
            canvasSign.gameObject.SetActive(true);
        }
        
    }

    public void loadLogin()
    {
        SceneManager.LoadScene("login");
    }

    public void loadReg()
    {
        SceneManager.LoadScene("regUser");
    }

    public void backMenu(){
        SceneManager.LoadScene("menu");
    }

    public void exit()
    {
        audioSource.Stop();
        Application.Quit();
    }


}
